import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterAsideComponent } from './footer-aside.component';

describe('FooterAsideComponent', () => {
  let component: FooterAsideComponent;
  let fixture: ComponentFixture<FooterAsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterAsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterAsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
